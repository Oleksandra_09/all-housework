

/*1.Чому для роботи з input не рекомендується використовувати клавіатуру?
 В основному через те, що функціонал перемішується з розміткою, що ускладнює роботу і налагодження коду,
  input запускається при будь-якій зміні значень(в клавіатурі цього немає),
  навіть тих, які не передбачають дії клавіатури: вставлення тексту за допомогою миші
  або використання розпізнавання мовлення для диктування тексту.
*/




//получаем список кнопок
let btns = document.querySelectorAll(".btn");

// присвоим кнопкам атрибуты

btns.forEach(function (element) {
    element.setAttribute('data-item', element.innerText);
});


document.addEventListener('keydown', function (event) {
    let clickNow = event.key; // присвоим переменной нажатие на конкретный элемент
    console.log(clickNow)
    btns.forEach(function (element) {
        element.style.backgroundColor = 'black'; 
        if ( element.dataset.item === clickNow || element.dataset.item === clickNow.toUpperCase() ) {
            element.style.backgroundColor = 'blue';
        }
    })
});
