


 const tabs = document.querySelector('.tabs');
 const liTabs = document.querySelectorAll(".tabs li"); 
 const text = document.querySelector(".tabs-content");
 const liText = document.querySelectorAll(".tabs-content li"); 
// скрываем текст 
   liText.forEach(function (element) {
    element.style.display = "none";
});
 //присваиваем data-атрибуты

 function setDataAttributes() {
    liTabs.forEach(function (element) {
        element.setAttribute('data-item', `${element.innerHTML}`); //для кнопок
    })

    let i = 0;
    liText.forEach(function (element) {
        element.setAttribute('data-text', `${liTabs[i].innerHTML}`);  // для текста
        i++;
    })
};
setDataAttributes();

   // делаем кнопки кликабельними
 tabs.addEventListener ("click" , function (event){
    liTabs.forEach(function (element) {
    if (element.classList.contains("active")){
        element.classList.remove("active");
    }
  });

   if(!event.target.classList.contains("active")){
    event.target.classList.add("active")
   };

  // присваеваем к кнопкам текст

   liTabs.forEach (function (element) {
    let textAndTab =  document.querySelector(`li[data-text ="${element.dataset.item}"]`)
    if(element.classList.contains("active")){
        textAndTab.style.display = "inline";
    }
    if (!element.classList.contains("active")){
        textAndTab.style.display = "none";
    }
})
});


