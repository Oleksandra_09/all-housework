


const main = document.querySelector('.main');
const styleMain = document.querySelector(".styleMain");
const butTem = document.querySelector(".btnTema");
const styleBtnTema = document.querySelector(".styleBtnTema");




butTem.addEventListener('click', function () {
    main.classList.toggle('styleMain');
    butTem.classList.toggle('styleBtnTema');
    sessionStorage.setItem('styleForMain', main.classList.contains('styleMain'));
    sessionStorage.setItem('styleForBut',  butTem.classList.contains('styleBtnTema'));
    });

window.onload = function () {  
    if(sessionStorage.getItem('styleForMain') === 'true' && sessionStorage.getItem('styleForBut') === 'true'){
        main.classList.add('styleMain');
        butTem.classList.add('styleBtnTema');
    };


};
