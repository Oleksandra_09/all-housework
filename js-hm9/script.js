/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
За допомогою метода createElement, він дозволяє створити новий елемент.
2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр функції означає позицію елемента,що додаємо,відносно того улемента,що викликає метод.
'beforebegin': до самого element.
'afterbegin':відразу після відкриваючого тега element.
'beforeend': відразу перед закриваючим тегом element.
'afterend': після element (після закриваючего тега).
3.Як можна видалити елемент зі сторінки? 
За допомоною метода  remove().
*/

function createList (array, parent = document.body ) {

  let ul = document.createElement('ul');
  ul.innerText = "list";
  parent.prepend(ul);
  array.forEach(function (element){
    const li = document.createElement('li')
    li.innerText = `${element}`;
    ul.append(li);
  })
  
};

createList(["ob", "fg", "fhg", "sea", "user" , 23]);
