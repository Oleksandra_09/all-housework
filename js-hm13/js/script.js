
/*1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
 setTimeout дозволяє нам запускати функцію один раз через певний інтервал часу,а setInterval дозволяє нам запускати функцію багаторазово,
починаючи через певний інтервал часу і кожний новий визов функції буде через цей інтервал часу

2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
 Якщо передати до setTimeout нульову затримку то функція відпрацює одразу після завершення виконання поточного скрипту, 
тобто якщо після setTimeout наприклад буде consolе.log() то спочатку відображаеться consolе.log а потім setTimeout.

3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Функція посилається на зовнішнє лексичне середовище, тому, поки вона живе, зовнішні змінні також живуть.
Вони можуть зайняти набагато більше пам’яті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати,
 навіть якщо вона дуже мала

*/


//создаем и стилизируем кнопку  "Припинити"

let button = document.createElement("button");
document.body.appendChild(button);
button.style.MozBorderRadius = '10px';
button.style.WebkitBorderRadius = '10px';
button.innerText = "Припинити";
button.style.color = "red";
button.style.margin = '20px';

//создаем и стилизируем кнопку  "Відновити показ"
let button1 = document.createElement("button");
document.body.appendChild(button1);
button1.style.MozBorderRadius = '10px';
button1.style.WebkitBorderRadius = '10px';
button1.innerText = "Відновити показ";
button1.style.color = "blue";
button1.style.left = '40px';
button1.style.margin = '20px';


// делаем слайдшоу
let slideIndex = 0;
function showImage() {
    let i;
    let slides = document.getElementsByClassName("image-to-show");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    };
    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1
    };
    slides[slideIndex - 1].style.display = "block";

};
showImage();
let interval = setInterval(showImage, 3000);

button.addEventListener("click", function (event) {
    if (event.target) {
        clearInterval( interval);
    }

});

button1.addEventListener("click", function (event) {
    if (event.target) {
        interval = setInterval(showImage, 3000);
    }

});


